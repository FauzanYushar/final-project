const { GAME } = require("../models");

const createGame = async (reg, res) => {
  try {
    const response = await GAME.create(reg.body);
    res.status(200).json({
      status: "Selamat, anda berhasil membuat game",
      data: response,
    });
  } catch (err) {
    res.status(400).json({
      status: "Maaf, anda tidak berhasil membuat game",
      msg: err,
    });
  }
};

const getGame = async (reg, res) => {
  try {
    const response = await GAME.findAll();
    res.status(200).json({
      status: "Berhasil mengambil semua data game",
      data: response,
    });
  } catch (error) {
    res.status(500).json({
      status: "Gagal mengambil data game",
      data: error.message,
    });
  }
};

const getGameById = (reg, res) => {
  GAME.findOne({ where: { id: reg.params.id } })
    .then((game) => {
      res.status(200).json({
        message: "Berhasil mengambil data game",
        data: game,
      });
    })
    .catch((err) => {
      res.json({
        message: "Gagal mengambil data game",
      });
    });
};

module.exports = { createGame, getGame, getGameById };
