const { USER } = require("../models");

const format = (user) => {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
};

exports.submit = (req, res) => {
  USER.authenticate(req.body)
    .then((user) => {
      res.status(200).json({ message: "Login Success", data: format(user) });
    })
    .catch((err) => {
      res.status(400).json({ message: err });
    });
};
