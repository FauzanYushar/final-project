const { USER } = require("../models");

exports.getAll = (req, res) => {
  USER.findAll()
    .then((data) => {
      res.status(200).json({ message: "get data users success", data: data });
    })
    .catch(() => {
      res.status(400).json({ message: "get data users failed" });
    });
};

exports.getOne = async (req, res) => {
  const { id } = req.params;
  const user = await USER.findOne({ where: { id } });
  if (user) {
    res.status(200).json({ message: "get user data success", data: user });
  } else res.status(400).json({ message: "data empty" });
};
exports.update = async (req, res) => {
  const { id } = req.params;
  const { name, username, email } = req.body;
  const usernameFound = await USER.findOne({ where: { username } });
  const emailFound = await USER.findOne({ where: { email } });
  const idFound = await USER.findOne({ where: { id } });

  if (
    (!usernameFound && !emailFound) ||
    (idFound.username == username && idFound.email == email)
  ) {
    USER.findOne({ where: { id } })
      .then((user) => {
        user
          .update({ name, username, email })
          .then((data) => {
            res.status(200).json({ message: "Update success", data: data });
          })
          .catch(() => {
            res.status(400).json({ message: "Update failed" });
          });
      })
      .catch(() => {
        res.status(400).json({ message: "Update failed" });
      });
  } else if (usernameFound && emailFound) {
    res.status(400).json({ message: "Username and Email already used" });
  } else if (emailFound) {
    res.status(400).json({ message: "Email already used" });
  } else res.status(400).json({ message: "Username already used" });
};

exports.delete = (req, res) => {
  const { id } = req.params;
  USER.destroy({ where: { id } })
    .then(() => {
      res.status(200).json({ message: "delete success" });
    })
    .catch(() => {
      res.status(400).json({ message: "delete failed" });
    });
};
