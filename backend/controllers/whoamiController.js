exports.get = (req, res) => {
  const currentUser = req.user;
  res.json(currentUser);
};
