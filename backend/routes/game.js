const router = require("express").Router();
const game = require("../controllers/gameController");
const start = require("../controllers/startController");

router.post("/", game.createGame);
router.get("/", game.getGame);
router.post("/startgame", start.startGame);
router.get("/:id", game.getGameById);

module.exports = router;
