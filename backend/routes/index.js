const express = require("express");
const router = express.Router();
const registerController = require("../controllers/registerController.js");
const loginController = require("../controllers/loginController.js");
const userRouter = require("./userRouter");
const restrict = require("../middlewares/restrict");
const whoamiController = require("../controllers/whoamiController");

const gameRouter = require("./game");
const historyRouter = require("./history");

router.post("/register", registerController.submit);
router.post("/login", loginController.submit);
router.use("/user", userRouter);
router.get("/whoami", restrict, whoamiController.get);

router.use("/game", gameRouter);
router.use("/history", historyRouter);

module.exports = router;
