const request = require("supertest");
const app = require("../app");

describe("Start Game API", () => {
  test("Menjalankan startgame API => object", async () => {
    return request(app)
    .post("/game/startgame")
    .send({
      user_id: 1,
      game_id: 1,
      choice: "gunting",
    })
    .expect(200)
  })
})