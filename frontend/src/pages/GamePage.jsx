import React from "react";
import { Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Game from "../components/game/Game";
import Navigation from "../components/Navigation";
import { getGame } from "../redux/games/reducer";
import dummy from "../utils/games";

const GamePage = () => {
  const {id} = useParams();
  const {detail} = useSelector((state) => state.games);
  const dispatch = useDispatch();
  const username = localStorage.getItem('username')

  React.useEffect(() => {
    dispatch(getGame(id))
  }, [dispatch, id])

  const gameDummy = dummy.find((row) => row.id === parseInt(id));

  return (
    <>
      <Navigation username={username} />
      <Container>
        <Game game={detail !== null ? detail : gameDummy} />
      </Container>
    </>
  );
};

export default GamePage;
