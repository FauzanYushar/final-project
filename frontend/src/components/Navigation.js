import React from "react";
import {
    Button,
    Navbar,
    Container,
    Nav,
} from "react-bootstrap";
import { useNavigate, Link } from "react-router-dom";

function Navigation() {
    const navigate = useNavigate();
    const username = localStorage.getItem('username');
    const accessToken = localStorage.getItem('accessToken');

    return (
        <Navbar bg="light" expand="lg">
            <Container expand="lg">
                <Navbar.Brand className="me-5">
                    <Link to="/" className="nav-link">
                        <img src="/icon/2.ico" alt="icon" width="50px" height="50px" />
                        .ico
                    </Link>
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }} >
                        <Link to="/" className="nav-link">Home</Link>
                        <Link to="/leaderboard" className="nav-link">Leaderboard</Link>
                    </Nav>
                    {accessToken ?
                        <Nav className="d-flex">
                            <Link to="/profile"
                                className="navbar-nav_profile_icon">
                                <img className=" rounded-circle" src={`https://ui-avatars.com/api/?name=${username}&background=random&size=50`} alt="profile avatar"></img>
                            </Link>
                        </Nav>
                        :
                        <Nav className="d-flex">
                            <Button
                                onClick={() => navigate('/login')}
                                className="me-3 game-btn-primary">Login</Button>
                            <Button
                                onClick={() => navigate('/register')}
                                className="game-btn-second">Register</Button>
                        </Nav>
                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Navigation;