import React, { useState } from "react";
import { Card, Button } from "react-bootstrap";
import { useNavigate, useOutletContext } from "react-router-dom";
import { BigPlayButton, Player } from "video-react";
import "../../styles/video.css";

const Hero = () => {
  const [play, setPlay] = useState();
  const navigate = useNavigate();
  const id = useOutletContext()[0];
  const cover = useOutletContext()[1];
  const btnStyle = {
    width: "250px",
    backgroundColor: "#65DBA3",
    color: "white",
    letterSpacing: ".1rem",
  };

  const handleClick = () => {
    navigate(`/game/${id}/start`);
  };

  return (
    <Card className="d-grid align-items-center p-0">
      <Player
        ref={(media) => {
          setPlay(media);
        }}
        poster="/images/cover.webp"
        src="/video/video.mp4"
        autoPlay={true}
      >
        <BigPlayButton position="center" />
      </Player>
      <div className="d-flex justify-content-center align-items-end">
        <Button
          style={btnStyle}
          className="text-uppercase shadow position-absolute mb-5 border-0 fs-3 fw-bold"
          onClick={handleClick}
          disabled={cover ? true : false}
        >
          Start Now
        </Button>
      </div>
    </Card>
  );
};

export default Hero;
