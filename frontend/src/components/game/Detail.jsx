import React from "react";
import { useOutletContext } from "react-router-dom";
import { Card } from "react-bootstrap";

const Detail = () => {
  const cardStyle = { height: "500px" };
  const detail = useOutletContext()[2];

  return (
    <Card.Body style={cardStyle} className="d-grid align-items-center">
      <Card.Text style={{ color: '#41B07B'}} className="text-center fw-bold p-5 fs-3">
        {detail}
      </Card.Text>
    </Card.Body>
  );
};

export default Detail;
