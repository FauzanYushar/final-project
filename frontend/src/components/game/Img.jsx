import React from "react";
import { Carousel } from "react-bootstrap";

const Img = () => {
  const imgs = ["/images/img1.jpg", "/images/img2.jpg", "/images/cover.webp"];
  const carouselStyle = {
    backgroundColor: "#DDDDDD",
    height: "200px",
  };
  const imgStyle = {
    width: "100%",
    maxHeight: "200px",
    objectFit: "cover",
    objectPosition: "center",
  };
  return (
    <Carousel style={carouselStyle}>
      {imgs.map((img, i) => {
        return (
          <Carousel.Item key={i} interval={1000} className="text-center">
            <img
              style={imgStyle}
              className="d-block"
              src={img}
              alt={`slide ${i}`}
            />
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
};

export default Img;
