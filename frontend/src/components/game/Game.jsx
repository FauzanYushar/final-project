import React from "react";
import { Card } from "react-bootstrap";
import { Outlet } from "react-router-dom";
import Img from "./Img";
import Menu from "./Menu";

const Game = ({ game }) => {
  const cardStyle = {
    backgroundColor: "#DEF1E0",
  };

  return (
    <>
      <Card style={cardStyle} className="my-4 p-0 border-0">
        <Img />
        <Menu id={game?.id}/>
        <Outlet context={[game?.id, game?.cover, game?.detail, game?.guide]} />    
      </Card>
    </>
  );
};

export default Game;
