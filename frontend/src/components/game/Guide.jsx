import React from "react";
import { useOutletContext } from "react-router-dom";
import { Card } from "react-bootstrap";

const Guide = () => {
  const cardStyle = { height: "500px" };
  const guide = useOutletContext()[3];

  return (
    <Card.Body style={cardStyle} className="d-grid align-items-center">
      <Card.Title
        style={{ color: "#41B07B" }}
        className="text-center fs-1 fw-bold"
      >
        {guide}
      </Card.Title>
    </Card.Body>
  );
};

export default Guide;
