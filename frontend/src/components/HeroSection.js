import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';

function HeroSection({ games }) {

    const featuredGame = games?.filter((game, index) => index < 3);
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex) => {
        setIndex(selectedIndex);
    };

    return (
        <Carousel activeIndex={index} onSelect={handleSelect}>
            {featuredGame?.map(game => (
                <Carousel.Item key={game?.id}>
                    <img
                        id="imgSlide"
                        className="w-100"
                        src={game?.cover?.includes('.jpg') ? `images/${game?.cover}` : "images/cover.webp"}
                        alt={game?.name}
                    />
                    <Carousel.Caption>
                        <h3>{game?.name}</h3>
                    </Carousel.Caption>
                </Carousel.Item>
            ))}
        </Carousel>
    );
}

export default HeroSection;