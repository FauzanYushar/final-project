import React from "react";
import { Card, Form, Button } from "react-bootstrap";

const AddGameInput = ({ addGame, games }) => {
  const [name, setName] = React.useState("");
  const [detail, setDetail] = React.useState("");
  const [guide, setGuide] = React.useState("");

  return (
    <Card style={{ backgroundColor: "#41B07B" }} className=" border-0 shadow">
      <Card.Header
        style={{ color: "#F2FFF3" }}
        className="text-center fw-bold fs-2 text-uppercase border-0 shadow"
      >
        Add Game
      </Card.Header>
      <Card.Body>
        <Form>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Control
              style={{ backgroundColor: "#F2FFF3", color: "#41B07B" }}
              type="text"
              placeholder="Judul Game"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Control
              style={{ backgroundColor: "#F2FFF3", color: "#41B07B" }}
              as="textarea"
              rows={2}
              placeholder="Detail Game"
              value={detail}
              onChange={(e) => setDetail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Control
              style={{ backgroundColor: "#F2FFF3", color: "#41B07B" }}
              as="textarea"
              rows={2}
              placeholder="Guide Game"
              value={guide}
              onChange={(e) => setGuide(e.target.value)}
            />
          </Form.Group>
        </Form>
      </Card.Body>
      <Card.Footer className="text-center border-0">
        <Button
          disabled={games?.length >= 10 ? true : false}
          style={{ backgroundColor: "#F2FFF3", color: "#41B07B" }}
          className=" rounded-pill border-0 fw-bold"
          onClick={() => addGame({ name, detail, guide })}
        >
          Add New Game
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default AddGameInput;
