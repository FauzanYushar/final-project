import React from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

const ButtonAdd = () => {
  const navigate = useNavigate();
  return (
    <Button
      style={{
        width: "50px",
        height: "50px",
        position: "fixed",
        zIndex: 100,
        bottom: "40px",
        right: "40px",
        backgroundColor: "#41B07B",
      }}
      className="text-center fs-4 rounded-circle border-0 shadow"
      onClick={() => navigate("/game/add")}
    >
      +
    </Button>
  );
};

export default ButtonAdd;
