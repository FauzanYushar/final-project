import React, { useState } from 'react';
import { Col, Row, Toast, ToastContainer } from 'react-bootstrap';

const ToastAlert = ({ message }) => {
  const [show, setShow] = useState(true);

  return (
    <Row>
      <Col xs={6}>
        <ToastContainer position="middle-center">
          <Toast onClose={() => setShow(false)} show={show} delay={5000} autohide>
            <Toast.Header>
              <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
              <strong className="me-auto">Warning!</strong>
              <small>connection refused</small>
            </Toast.Header>
            <Toast.Body>
              {message} Try again later.
            </Toast.Body>
          </Toast>
        </ToastContainer>
      </Col>
    </Row>
  );
};

export default ToastAlert;
