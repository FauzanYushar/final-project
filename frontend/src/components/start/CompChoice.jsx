import React from "react";
import { Card } from "react-bootstrap";

const CompChoice = ({ choices, headerStyle, choiceStyle, comp }) => {  
  return (
    <Card className=" bg-transparent border-0">
      <Card.Header
        style={headerStyle}
        className="text-center fs-1 fw-bold text-uppercase bg-transparent border-0"
      >
        Comp
      </Card.Header>
      <Card.Body className="d-grid gap-4 justify-content-center">
        {choices.map((choice, i) => {
          return (
            <Card key={i} bg={comp === choice ? 'danger' : 'transparent' } className="border-0">
              <Card.Img  src={`/images/${choice}.png`} style={choiceStyle} className="p-4 img_choice" />
            </Card>
          );
        })}
      </Card.Body>
    </Card>
  );
};

export default CompChoice;
