import React from "react";
import { Button, Card } from "react-bootstrap";

const UserChoice = ({
  choices,
  headerStyle,
  choiceStyle,
  handleClick,
  round,
}) => {
  return (
    <Card className=" bg-transparent border-0">
      <Card.Header
        style={headerStyle}
        className="text-center fs-1 fw-bold text-uppercase bg-transparent border-0"
      >
        Player
      </Card.Header>
      <Card.Body className="d-grid gap-4 justify-content-center">
        {choices.map((choice, i) => {
          return (
            <Button
              key={i}
              className="bg-transparent border-0 p-0 choice"
              disabled={round === 3 ? true : false}
            >
              <Card.Img
                src={`/images/${choice}.png`}
                style={choiceStyle}
                className="p-4 img_choice"
                onClick={() => handleClick(choice)}
              />
            </Button>
          );
        })}
      </Card.Body>
    </Card>
  );
};

export default UserChoice;
