import React from "react";
import { useState, useEffect } from "react";
import Alert from "react-bootstrap/Alert";
import "./style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Lottie from "lottie-react";
import anims from "../../../assets/animations/24219-controller.json";
import { loginUser, data, reset } from "../../../redux/auth/reducer";

const Login = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [messagesLogin, setMessagesLogin] = useState("");
  const [loading, setLoading] = useState(false);
  const { message, isSuccess } = useSelector(data);

  const dispatch = useDispatch();

  const AuthRegister = () => {
    setTimeout(() => {
      navigate("/register");
    }, 1000);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const payload = { username, password };
    dispatch(loginUser(payload));
  };

  useEffect(() => {
    if (isSuccess) {
      setLoading(false);
      setMessagesLogin(message);
      setTimeout(() => {
        navigate("/");
        dispatch(reset());
      }, 3000);
    } else {
      setMessagesLogin(message);
      setTimeout(() => {
        dispatch(reset());
      }, 10000);
    }
  });

  return (
    <div className="login-page">
      <div className="header-login">
        <h1 className="header">PLAY MORE, FEEL LIVE MORE</h1>
      </div>
      <div className="form-login">
        <div className="box-login">
          <form>
            <div className="circle">
              <Lottie animationData={anims}></Lottie>
            </div>
            <input
              placeholder="Username"
              className="username-input"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <input
              placeholder="Password"
              className="password-input"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button
              variant="success"
              className="login-button"
              onClick={handleSubmit}
              type="submit"
              disabled={!username || !password}
            >
              LOGIN
            </Button>

            {messagesLogin && (
              <Alert variant="info" className="text-center w-50 mx-auto">
                {message}
              </Alert>
            )}
            {loading && (
              <Alert variant="light" className="text-center w-50 mx-auto">
                Loading
              </Alert>
            )}
            <p className="sign-up">
              Need an account?
              <Button className="btn-sign-up" onClick={AuthRegister}>
                SIGN UP
              </Button>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
