import React from "react";
import Navbar from "../navigation/index";
import Tabel from "../tabel/index";
import "./style.css";
const LeaderBoard = () => {
  return (
    <div className="leaderboard-container">
      <Navbar />
      <div className="container-tabel">
        <h1> LEADERBOARD</h1>
        <div className="tabel">
          <Tabel />
        </div>
      </div>
    </div>
  );
};

export default LeaderBoard;
