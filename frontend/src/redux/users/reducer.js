import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  getAllUserAPI,
  getUserByIdAPI,
  updateUserAPI,
  deleteUserAPI,
} from "./reducerAPI";

const initialState = {
  data: [],
  isLoading: false,
  error: "",
  status: "idle",
  detail: [],
};

export const getAllUser = createAsyncThunk("user/getAll", async () => {
  const response = await getAllUserAPI();
  return response.data.data;
});

export const getUserById = createAsyncThunk("user/getOne", async (id) => {
  const response = await getUserByIdAPI(id);
  return response.data.data;
});

export const updateUser = createAsyncThunk("user/update", async (payload) => {
  const response = await updateUserAPI(payload);
  return response.data.data;
});

export const deleteUser = createAsyncThunk("user/delete", async (id) => {
  const response = await deleteUserAPI(id);
  return response.message;
});

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(getAllUser.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getAllUser.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.detail = action.payload;
      })
      .addCase(getAllUser.rejected, (state) => {
        state.status = "failed";
        state.error = "Failed to fetch data!";
      })

      .addCase(getUserById.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getUserById.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.data = action.payload;
      })
      .addCase(getUserById.rejected, (state) => {
        state.status = "failed";
        state.error = "Failed to fetch data!";
      })

      .addCase(updateUser.pending, (state) => {
        state.status = "loading";
      })
      .addCase(updateUser.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.data = action.payload;
      })
      .addCase(updateUser.rejected, (state) => {
        state.status = "failed";
        state.error = "Failed to fetch data!";
      })

      .addCase(deleteUser.pending, (state) => {
        state.status = "loading";
      })
      .addCase(deleteUser.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.data = action.payload;
      })
      .addCase(deleteUser.rejected, (state) => {
        state.status = "failed";
        state.error = "Failed to fetch data!";
      });
  },
});

export const {} = userSlice.actions;
export const data = (state) => state.user;
export default userSlice.reducer;
