import axios from "axios";
import BASE_URL from "../../utils/baseUrl";

// coding disini ....
// untuk menampung API
export const getHistoryAPI = async (userId) => {
  const res = await axios.get(`${BASE_URL}/history/${userId}`);
  return res;
};

export const getAllHistoryAPI = async () => {
  const res = await axios.get(`${BASE_URL}/history`);
  return res;
};
