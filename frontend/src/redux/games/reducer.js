import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { addGameAPI, getGameAPI, getGamesAPI } from './reducerAPI';
import dummy from '../../utils/games';

const initialState = {
  data: [],
  detail: null,
  status: 'idle',
  error: null,
};

export const getGamesThunkFunc = createAsyncThunk('games/getGamesThunkFunc', async () => {
  const response = await getGamesAPI();
  return response.data.data;
});

export const getGame = createAsyncThunk('games/detail', async (id) => {
  const response = await getGameAPI(id);
  return response.data.data;
});

export const addGame = createAsyncThunk("games/add", async (payload) => {
  const response = await addGameAPI(payload);
  return response.data.data;
}) 

const gamesSlice = createSlice({
  name: 'games',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(getGamesThunkFunc.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getGamesThunkFunc.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.data = action.payload;
      })
      .addCase(getGamesThunkFunc.rejected, (state) => {
        state.status = 'failed';
        state.error = true;
        state.message = 'Failed to fetch data!';
        state.data = dummy;
      })
      .addCase(getGame.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getGame.fulfilled, (state, action) => {
        state.status = 'success';
        state.detail = action.payload;
      })
      .addCase(getGame.rejected, (state) => {
        state.status = 'failed';
      })
      .addCase(addGame.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(addGame.fulfilled, (state) => {
        state.status = 'success';
      })
      .addCase(addGame.rejected, (state) => {
        state.status = 'failed';
      })
  },
});

export const selectGameList = (state) => state.games.data;

export default gamesSlice.reducer;
