import axios from "axios";
import BASE_URL from "../../utils/baseUrl";

export const getGamesAPI = async () => {
  const response = await axios.get(`${BASE_URL}/game`);
  return response;
};

export const getGameAPI = async (id) => {
  const response = await axios.get(`${BASE_URL}/game/${id}`);
  return response;
};

export const addGameAPI = async (payload) => {
  const response = await axios.post(`${BASE_URL}/game`, payload);
  return response;
};
