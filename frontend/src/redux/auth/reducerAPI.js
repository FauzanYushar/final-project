import axios from "axios";
import BASE_URL from "../../utils/baseUrl";

// coding disini ....

export const loginPostAPI = async (payload) => {
  const res = await axios.post(`${BASE_URL}/login`, payload);
  return res;
};

export const postRegisterAPI = async (payload) => {
  const res = await axios.post(`${BASE_URL}/register`, payload);
  return res;
};
