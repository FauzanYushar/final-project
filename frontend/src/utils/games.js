const dummy = [
    {
        id: 1,
        name: 'Cyberpunk 2077',
        cover: 'cyberpunk.jpg',
        detail: 'Cyberpunk 2077 is an open-world, action-adventure story set in Night City, a megalopolis obsessed with power, glamour and body modification. You play as V, a mercenary outlaw going after a one-of-a-kind implant that is the key to immortality. You can customize your characters cyberware, skillset and playstyle, and explore a vast city where the choices you make shape the story and the world around you.',
        guide: 'SOON'
    },
    {
        id: 2,
        name: 'FIFA 2023',
        cover: 'FIFA_23.jpg',
        detail: 'Live out your football dreams as a manager or a player in Career Mode FIFA 23 brings you the ability to define your personality as a player, manage as some of footballs most famous names, and enjoy a new way to play your season with Playable Highlights in the most authentic FIFA Career Mode experience to date.',
        guide: 'SOON'
    },
    {
        id: 3,
        name: 'Red Dead Redemtion',
        cover: 'red-dead.jpg',
        detail: 'Red Dead Redemption is a 2010 action-adventure game developed by Rockstar San Diego and published by Rockstar Games. A spiritual successor to 2004 Red Dead Revolver, it is the second game in the Red Dead series. Red Dead Redemption is set during the decline of the American frontier in the year 1911 and follows John Marston, a former outlaw whose wife and son are taken hostage by the government in ransom for his services as a hired gun. Having no other choice, Marston sets out to bring three members of his former gang to justice.',
        guide: 'SOON'
    },
    {
        id: 4,
        name: 'Cyberpunk 2077',
        cover: 'cyberpunk.jpg',
        detail: 'Cyberpunk 2077 is an open-world, action-adventure story set in Night City, a megalopolis obsessed with power, glamour and body modification. You play as V, a mercenary outlaw going after a one-of-a-kind implant that is the key to immortality. You can customize your characters cyberware, skillset and playstyle, and explore a vast city where the choices you make shape the story and the world around you.',
        guide: 'SOON'
    },
    {
        id: 5,
        name: 'FIFA 2023',
        cover: 'FIFA_23.jpg',
        detail: 'Live out your football dreams as a manager or a player in Career Mode FIFA 23 brings you the ability to define your personality as a player, manage as some of footballs most famous names, and enjoy a new way to play your season with Playable Highlights in the most authentic FIFA Career Mode experience to date.',
        guide: 'SOON'
    },
    {
        id: 6,
        name: 'Red Dead Redemtion',
        cover: 'red-dead.jpg',
        detail: 'Red Dead Redemption is a 2010 action-adventure game developed by Rockstar San Diego and published by Rockstar Games. A spiritual successor to 2004 Red Dead Revolver, it is the second game in the Red Dead series. Red Dead Redemption is set during the decline of the American frontier in the year 1911 and follows John Marston, a former outlaw whose wife and son are taken hostage by the government in ransom for his services as a hired gun. Having no other choice, Marston sets out to bring three members of his former gang to justice.',
        guide: 'SOON'
    }
]

export default dummy;